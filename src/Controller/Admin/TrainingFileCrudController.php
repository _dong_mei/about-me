<?php

namespace App\Controller\Admin;

use App\Entity\TrainingFile;
use App\Field\VichFileField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Vich\UploaderBundle\Form\Type\VichFileType;

class TrainingFileCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return TrainingFile::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('title', 'Titre du fichier'),
            TextareaField::new('description'),
            ChoiceField::new('type', 'Type de fichier')
            ->setChoices([
                'Certification'=>'Certification',
                'Lettre de recommendation'=>'Lettre de recommendation',
                'Diplôme'=>'Diplôme',
                'Bulletin de notes'=>'Bulletin de notes',
                'Résultats d\'examen'=> 'Résultats d\'examen',
                "Autre"=>"Autre"
            ]),
            VichFileField::new('fileFile', 'Fichier')
        ];
    }
}
