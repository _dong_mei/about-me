<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220309204511 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE professional_experience_picture ADD professional_experience_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE professional_experience_picture ADD CONSTRAINT FK_E2A58FC958832594 FOREIGN KEY (professional_experience_id) REFERENCES professional_experience (id)');
        $this->addSql('CREATE INDEX IDX_E2A58FC958832594 ON professional_experience_picture (professional_experience_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE professional_experience_picture DROP FOREIGN KEY FK_E2A58FC958832594');
        $this->addSql('DROP INDEX IDX_E2A58FC958832594 ON professional_experience_picture');
        $this->addSql('ALTER TABLE professional_experience_picture DROP professional_experience_id');
    }
}
