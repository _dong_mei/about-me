<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220311073232 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE professional_experience ADD slider_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE professional_experience ADD CONSTRAINT FK_32FDB9BA2CCC9638 FOREIGN KEY (slider_id) REFERENCES professional_experience_slider (id)');
        $this->addSql('CREATE INDEX IDX_32FDB9BA2CCC9638 ON professional_experience (slider_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE professional_experience DROP FOREIGN KEY FK_32FDB9BA2CCC9638');
        $this->addSql('DROP INDEX IDX_32FDB9BA2CCC9638 ON professional_experience');
        $this->addSql('ALTER TABLE professional_experience DROP slider_id');
    }
}
