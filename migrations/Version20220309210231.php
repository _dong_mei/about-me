<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220309210231 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE other_experience (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(255) NOT NULL, start_date DATE NOT NULL, end_date DATE DEFAULT NULL, description LONGTEXT DEFAULT NULL, place VARCHAR(255) DEFAULT NULL, link VARCHAR(400) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE other_experience_picture ADD other_experience_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE other_experience_picture ADD CONSTRAINT FK_A775885770EE9221 FOREIGN KEY (other_experience_id) REFERENCES other_experience (id)');
        $this->addSql('CREATE INDEX IDX_A775885770EE9221 ON other_experience_picture (other_experience_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE other_experience_picture DROP FOREIGN KEY FK_A775885770EE9221');
        $this->addSql('DROP TABLE other_experience');
        $this->addSql('DROP INDEX IDX_A775885770EE9221 ON other_experience_picture');
        $this->addSql('ALTER TABLE other_experience_picture DROP other_experience_id');
    }
}
