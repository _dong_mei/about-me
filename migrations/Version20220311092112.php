<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220311092112 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE other_experience_other_experience_file (other_experience_id INT NOT NULL, other_experience_file_id INT NOT NULL, INDEX IDX_77D9B2BD70EE9221 (other_experience_id), INDEX IDX_77D9B2BD2265BEB2 (other_experience_file_id), PRIMARY KEY(other_experience_id, other_experience_file_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE other_experience_other_experience_file ADD CONSTRAINT FK_77D9B2BD70EE9221 FOREIGN KEY (other_experience_id) REFERENCES other_experience (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE other_experience_other_experience_file ADD CONSTRAINT FK_77D9B2BD2265BEB2 FOREIGN KEY (other_experience_file_id) REFERENCES other_experience_file (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE other_experience_other_experience_file');
    }
}
