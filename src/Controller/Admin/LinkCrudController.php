<?php

namespace App\Controller\Admin;

use App\Entity\Link;
use App\Field\VichImageField;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\UrlField;

class LinkCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Link::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('title', 'Nom du lien'),
            UrlField::new('link', 'URL du lien'),
            ChoiceField::new('type', 'Type de lien')
            ->setChoices([
                'email'=>'email',
                'numéro de téléphone' => 'numéro de téléphone',
                'site web'=>'site web'
            ]),
            VichImageField::new('imageFile', 'Icon')
        ];
    }
    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInSingular('Lien')
            ->setEntityLabelInPlural('Liens');
    }

}
