<?php

namespace App\Entity;

use App\Repository\OtherExperienceSliderRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=OtherExperienceSliderRepository::class)
 */
class OtherExperienceSlider
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\ManyToMany(targetEntity=OtherExperiencePicture::class, inversedBy="otherExperienceSliders")
     */
    private $picture;

    /**
     * @ORM\OneToMany(targetEntity=OtherExperience::class, mappedBy="slider")
     */
    private $otherExperiences;

    public function __construct()
    {
        $this->picture = new ArrayCollection();
        $this->otherExperiences = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return Collection<int, OtherExperiencePicture>
     */
    public function getPicture(): Collection
    {
        return $this->picture;
    }

    public function addPicture(OtherExperiencePicture $picture): self
    {
        if (!$this->picture->contains($picture)) {
            $this->picture[] = $picture;
        }

        return $this;
    }

    public function removePicture(OtherExperiencePicture $picture): self
    {
        $this->picture->removeElement($picture);

        return $this;
    }

    /**
     * @return Collection<int, OtherExperience>
     */
    public function getOtherExperiences(): Collection
    {
        return $this->otherExperiences;
    }

    public function addOtherExperience(OtherExperience $otherExperience): self
    {
        if (!$this->otherExperiences->contains($otherExperience)) {
            $this->otherExperiences[] = $otherExperience;
            $otherExperience->setSlider($this);
        }

        return $this;
    }

    public function removeOtherExperience(OtherExperience $otherExperience): self
    {
        if ($this->otherExperiences->removeElement($otherExperience)) {
            // set the owning side to null (unless already changed)
            if ($otherExperience->getSlider() === $this) {
                $otherExperience->setSlider(null);
            }
        }

        return $this;
    }
}
