<?php

namespace App\Controller\Admin;

use App\Entity\ProfessionalExperience;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\UrlField;

class ProfessionalExperienceCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return ProfessionalExperience::class;
    }


    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('jobTitle', 'Titre du poste'),
            TextField::new('compagnyName', 'Nom de l\'entreprise'),
            UrlField::new('compagnyWebSite', 'Site de l\'entreprise'),
            DateField::new('startDate', 'Date de début du contrat'),
            DateField::new('endDate', 'Date de fin du contrat'),
            ChoiceField::new('dimension', 'Type de contrat')
            ->setChoices([
                'stage'=>'stage',
                'alternance'=>'alternance',
                'interim'=>'interim',
                'emploi'=>'emploi',
            ]),
            TextEditorField::new('description', 'Description du poste'),
            AssociationField::new('slider', 'Slider'),
            AssociationField::new('file', 'Fichiers')
        ];
    }
    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInSingular('Expérience Professionnelle')
            ->setEntityLabelInPlural('Expériences Professionnelles');
    }
}
