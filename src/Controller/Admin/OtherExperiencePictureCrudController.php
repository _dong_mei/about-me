<?php

namespace App\Controller\Admin;

use App\Entity\OtherExperiencePicture;
use App\Field\VichImageField;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class OtherExperiencePictureCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return OtherExperiencePicture::class;
    }


    public function configureFields(string $pageName): iterable
    {
        return [
            VichImageField::new('imageFile', 'Image'),
        ];
    }
    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInSingular('Image')
            ->setEntityLabelInPlural('Images');
    }
}
