<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220310214435 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE training_slider (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE training_picture ADD training_slider_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE training_picture ADD CONSTRAINT FK_CBE5DF874240F5CC FOREIGN KEY (training_slider_id) REFERENCES training_slider (id)');
        $this->addSql('CREATE INDEX IDX_CBE5DF874240F5CC ON training_picture (training_slider_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE training_picture DROP FOREIGN KEY FK_CBE5DF874240F5CC');
        $this->addSql('DROP TABLE training_slider');
        $this->addSql('DROP INDEX IDX_CBE5DF874240F5CC ON training_picture');
        $this->addSql('ALTER TABLE training_picture DROP training_slider_id');
    }
}
