<?php

namespace App\Controller\Admin;

use App\Entity\Training;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\UrlField;
use http\Url;

class TrainingCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Training::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('title', 'Titre de la formation'),
            DateField::new('startDate', 'Date de début de formation'),
            DateField::new('endDate', 'Date de fin de formation'),
            ChoiceField::new('dimension', 'Cursus')
            ->setChoices([
                'études'=>'études',
                'autodidacte'=>'autodidacte',
                'formation d\'entreprise'=>'formation d\'entreprise'
            ]),
            TextEditorField::new('description', 'Description'),
            TextField::new('schoolName', 'Nom de l\'organisme de formation'),
            UrlField::new('schoolWebSite', 'Site Web de l\'organisme de formation'),
            TextField::new('place', 'Lieu de formation'),
            TextField::new('compagny', 'Nom de l\'entreprise partenaire'),
            UrlField::new('compagnyWebSite', 'Site Web de l\'entreprise partenaire'),
            AssociationField::new('slider', 'Slider'),
            AssociationField::new('file', 'Fichiers')
        ];
    }
    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInSingular('Formation')
            ->setEntityLabelInPlural('Formations');
    }
}
