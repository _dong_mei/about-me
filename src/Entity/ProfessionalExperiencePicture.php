<?php

namespace App\Entity;

use App\Repository\ProfessionalExperiencePictureRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass=ProfessionalExperiencePictureRepository::class)
 * @Vich\Uploadable
 */
class ProfessionalExperiencePicture
{
    public function __toString(): string
    {
        return $this->getImage();
    }
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    private $image;

    /**
     * @Vich\UploadableField(mapping="professionalExperience_images", fileNameProperty="image")
     * @var File
     */
    private $imageFile;

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private $updatedAt;

    /**
     * @ORM\ManyToMany(targetEntity=ProfessionalExperienceSlider::class, mappedBy="picture")
     */
    private $professionalExperienceSliders;

    public function __construct()
    {
        $this->professionalExperienceSliders = new ArrayCollection();
    }

    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;

        // VERY IMPORTANT:
        // It is required that at least one field changes if you are using Doctrine,
        // otherwise the event listeners won't be called and the file is lost
        if ($image) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->updatedAt = new \DateTime('now');
        }
    }

    public function getImageFile()
    {
        return $this->imageFile;
    }

    public function setImage($image)
    {
        $this->image = $image;
    }

    public function getImage()
    {
        return $this->image;
    }

    public function getUpdatedAt(){
        return $this->updatedAt;
    }

    /**
     * @return Collection<int, ProfessionalExperienceSlider>
     */
    public function getProfessionalExperienceSliders(): Collection
    {
        return $this->professionalExperienceSliders;
    }

    public function addProfessionalExperienceSlider(ProfessionalExperienceSlider $professionalExperienceSlider): self
    {
        if (!$this->professionalExperienceSliders->contains($professionalExperienceSlider)) {
            $this->professionalExperienceSliders[] = $professionalExperienceSlider;
            $professionalExperienceSlider->addPicture($this);
        }

        return $this;
    }

    public function removeProfessionalExperienceSlider(ProfessionalExperienceSlider $professionalExperienceSlider): self
    {
        if ($this->professionalExperienceSliders->removeElement($professionalExperienceSlider)) {
            $professionalExperienceSlider->removePicture($this);
        }

        return $this;
    }
}
