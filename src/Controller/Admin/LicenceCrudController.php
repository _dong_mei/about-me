<?php

namespace App\Controller\Admin;

use App\Entity\Licence;
use App\Field\VichImageField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class LicenceCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Licence::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('title', 'Nom du permis'),
            BooleanField::new('vehicle', 'Véhicule personnel ? '),
            VichImageField::new('imageFile', 'Icon du type de permis')
        ];
    }

}
