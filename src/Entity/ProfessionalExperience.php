<?php

namespace App\Entity;

use App\Repository\ProfessionalExperienceRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ProfessionalExperienceRepository::class)
 */
class ProfessionalExperience
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $jobTitle;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $compagnyName;

    /**
     * @ORM\Column(type="string", length=500, nullable=true)
     */
    private $compagnyWebSite;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $place;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $startDate;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $endDate;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $dimension;

    /**
     * @ORM\ManyToOne(targetEntity=ProfessionalExperienceSlider::class, inversedBy="professionalExperiences")
     */
    private $slider;

    /**
     * @ORM\ManyToMany(targetEntity=ProfessionalExperienceFile::class, inversedBy="professionalExperiences")
     */
    private $file;

    public function __construct()
    {
        $this->file = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getJobTitle(): ?string
    {
        return $this->jobTitle;
    }

    public function setJobTitle(string $jobTitle): self
    {
        $this->jobTitle = $jobTitle;

        return $this;
    }

    public function getCompagnyName(): ?string
    {
        return $this->compagnyName;
    }

    public function setCompagnyName(string $compagnyName): self
    {
        $this->compagnyName = $compagnyName;

        return $this;
    }

    public function getCompagnyWebSite(): ?string
    {
        return $this->compagnyWebSite;
    }

    public function setCompagnyWebSite(?string $compagnyWebSite): self
    {
        $this->compagnyWebSite = $compagnyWebSite;

        return $this;
    }

    public function getPlace(): ?string
    {
        return $this->place;
    }

    public function setPlace(?string $place): self
    {
        $this->place = $place;

        return $this;
    }

    public function getStartDate(): ?\DateTimeInterface
    {
        return $this->startDate;
    }

    public function setStartDate(?\DateTimeInterface $startDate): self
    {
        $this->startDate = $startDate;

        return $this;
    }

    public function getEndDate(): ?\DateTimeInterface
    {
        return $this->endDate;
    }

    public function setEndDate(?\DateTimeInterface $endDate): self
    {
        $this->endDate = $endDate;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getDimension(): ?string
    {
        return $this->dimension;
    }

    public function setDimension(string $dimension): self
    {
        $this->dimension = $dimension;

        return $this;
    }

    public function getSlider(): ?ProfessionalExperienceSlider
    {
        return $this->slider;
    }

    public function setSlider(?ProfessionalExperienceSlider $slider): self
    {
        $this->slider = $slider;

        return $this;
    }

    /**
     * @return Collection<int, ProfessionalExperienceFile>
     */
    public function getFile(): Collection
    {
        return $this->file;
    }

    public function addFile(ProfessionalExperienceFile $file): self
    {
        if (!$this->file->contains($file)) {
            $this->file[] = $file;
        }

        return $this;
    }

    public function removeFile(ProfessionalExperienceFile $file): self
    {
        $this->file->removeElement($file);

        return $this;
    }
}
