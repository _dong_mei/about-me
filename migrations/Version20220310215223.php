<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220310215223 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE training ADD slider_id INT DEFAULT NULL, ADD start_date DATE NOT NULL, ADD end_date DATE NOT NULL, ADD title VARCHAR(255) NOT NULL, ADD dimension VARCHAR(255) NOT NULL, ADD school_name VARCHAR(255) DEFAULT NULL, ADD place VARCHAR(255) DEFAULT NULL, ADD school_web_site VARCHAR(255) DEFAULT NULL, ADD description LONGTEXT DEFAULT NULL, ADD compagny VARCHAR(255) DEFAULT NULL, ADD compagny_web_site VARCHAR(255) DEFAULT NULL, ADD results_obtained VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE training ADD CONSTRAINT FK_D5128A8F2CCC9638 FOREIGN KEY (slider_id) REFERENCES training_slider (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_D5128A8F2CCC9638 ON training (slider_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE training DROP FOREIGN KEY FK_D5128A8F2CCC9638');
        $this->addSql('DROP INDEX UNIQ_D5128A8F2CCC9638 ON training');
        $this->addSql('ALTER TABLE training DROP slider_id, DROP start_date, DROP end_date, DROP title, DROP dimension, DROP school_name, DROP place, DROP school_web_site, DROP description, DROP compagny, DROP compagny_web_site, DROP results_obtained');
    }
}
