<?php

namespace App\Controller\Admin;

use App\Entity\Contact;
use App\Entity\Hobby;
use App\Entity\Language;
use App\Entity\Licence;
use App\Entity\Link;
use App\Entity\OtherExperience;
use App\Entity\OtherExperienceFile;
use App\Entity\OtherExperiencePicture;
use App\Entity\OtherExperienceSlider;
use App\Entity\ProfessionalExperience;
use App\Entity\ProfessionalExperienceFile;
use App\Entity\ProfessionalExperiencePicture;
use App\Entity\ProfessionalExperienceSlider;
use App\Entity\Profile;
use App\Entity\Project;
use App\Entity\Skill;
use App\Entity\SkillsCategory;
use App\Entity\Training;
use App\Entity\TrainingFile;
use App\Entity\TrainingPicture;
use App\Entity\TrainingSlider;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminDashboardController extends AbstractDashboardController
{
    #[Route('/admin', name: 'admin')]
    public function index(): Response
    {

        // Option 1. You can make your dashboard redirect to some common page of your backend
        //
        $adminUrlGenerator = $this->container->get(AdminUrlGenerator::class);
        return $this->redirect($adminUrlGenerator->setController(ProfileCrudController::class)->generateUrl());

        // Option 2. You can make your dashboard redirect to different pages depending on the user
        //
        // if ('jane' === $this->getUser()->getUsername()) {
        //     return $this->redirect('...');
        // }

        // Option 3. You can render some custom template to display a proper dashboard with widgets, etc.
        // (tip: it's easier if your template extends from @EasyAdmin/page/content.html.twig)
        //
        // return $this->render('some/path/my-dashboard.html.twig');
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('<img src="/assets/admin-logo.png" width="100%">')
            ->setFaviconPath('/assets/logo.png')
            ->renderContentMaximized();
    }

    public function configureMenuItems(): iterable
    {
        return[
            MenuItem::section('Cible'),
            MenuItem::linkToCrud('Profile','fa fa-user', Profile::class ),
            MenuItem::linkToCrud('Permis', 'fa fa-truck-pickup', Licence::class ),
            MenuItem::linkToCrud('Adresses', 'fa fa-map-marked-alt', Contact::class),
            MenuItem::linkToCrud('Liens', 'fa fa-link', Link::class),
            MenuItem::linkToCrud('Centres d\'intérêt','fa fa-paint-brush', Hobby::class ),
            MenuItem::section('Acquis'),
            MenuItem::linkToCrud('Catégories de compétences','fa fa-stream', SkillsCategory::class ),
            MenuItem::linkToCrud('Compétences','fa fa-check-square', Skill::class ),
            MenuItem::linkToCrud('Langues','fa fa-comments', Language::class ),
            MenuItem::section('Parcours'),
            MenuItem::subMenu('Formations', 'fa fa-school')->setSubItems([
                MenuItem::linkToCrud('Images des sliders de formations','fa fa-camera', TrainingPicture::class ),
                MenuItem::linkToCrud('Sliders de formations','fa fa-images', TrainingSlider::class ),
                MenuItem::linkToCrud('Formations','fa fa-school', Training::class ),
                MenuItem::linkToCrud('Fichiers', 'fa fa-file', TrainingFile::class)
            ]),
            MenuItem::subMenu('Expériences Professionnelles', 'fa fa-briefcase')->setSubItems([
                MenuItem::linkToCrud('Images des sliders d\'expériences professionnelles','fa fa-camera', ProfessionalExperiencePicture::class ),
                MenuItem::linkToCrud('Sliders d\'expériences professionnelles','fa fa-images', ProfessionalExperienceSlider::class ),
                MenuItem::linkToCrud('Expériences Professionnelles','fa fa-briefcase', ProfessionalExperience::class ),
                MenuItem::linkToCrud('Fichiers', 'fa fa-file', ProfessionalExperienceFile::class)
            ]),
            MenuItem::subMenu('Expériences Annexes', 'fa fa-hands-helping')->setSubItems([
                MenuItem::linkToCrud('Images des sliders d\'expériences annexes','fa fa-camera', OtherExperiencePicture::class ),
                MenuItem::linkToCrud('Sliders d\'expériences annexes','fa fa-images', OtherExperienceSlider::class ),
                MenuItem::linkToCrud('Exprériences Annexes','fa fa-hands-helping', OtherExperience::class ),
                MenuItem::linkToCrud('Fichiers', 'fa fa-file', OtherExperienceFile::class)
            ]),
            MenuItem::section('Portfolio'),
            MenuItem::linkToCrud('Projets', 'fa fa-hammer', Project::class),
        ];

    }
}
