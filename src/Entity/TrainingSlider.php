<?php

namespace App\Entity;

use App\Repository\TrainingSliderRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JetBrains\PhpStorm\Pure;

/**
 * @ORM\Entity(repositoryClass=TrainingSliderRepository::class)
 */
class TrainingSlider
{
    #[Pure] public function __toString(): string
    {
        return $this->getTitle();
    }
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\ManyToMany(targetEntity=TrainingPicture::class, inversedBy="trainingSliders")
     */
    private $picture;

    #[Pure] public function __construct()
    {
        $this->picture = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return Collection<int, TrainingPicture>
     */
    public function getPicture(): Collection
    {
        return $this->picture;
    }

    public function addPicture(TrainingPicture $picture): self
    {
        if (!$this->picture->contains($picture)) {
            $this->picture[] = $picture;
        }

        return $this;
    }

    public function removePicture(TrainingPicture $picture): self
    {
        $this->picture->removeElement($picture);

        return $this;
    }
}
