<?php

namespace App\Entity;

use App\Repository\ProfessionalExperienceSliderRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ProfessionalExperienceSliderRepository::class)
 */
class ProfessionalExperienceSlider
{
    public function __toString(): string
    {
        return $this->getTitle();
    }
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\ManyToMany(targetEntity=ProfessionalExperiencePicture::class, inversedBy="professionalExperienceSliders")
     */
    private $picture;

    /**
     * @ORM\OneToMany(targetEntity=ProfessionalExperience::class, mappedBy="slider")
     */
    private $professionalExperiences;

    public function __construct()
    {
        $this->picture = new ArrayCollection();
        $this->professionalExperiences = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return Collection<int, ProfessionalExperiencePicture>
     */
    public function getPicture(): Collection
    {
        return $this->picture;
    }

    public function addPicture(ProfessionalExperiencePicture $picture): self
    {
        if (!$this->picture->contains($picture)) {
            $this->picture[] = $picture;
        }

        return $this;
    }

    public function removePicture(ProfessionalExperiencePicture $picture): self
    {
        $this->picture->removeElement($picture);

        return $this;
    }

    /**
     * @return Collection<int, ProfessionalExperience>
     */
    public function getProfessionalExperiences(): Collection
    {
        return $this->professionalExperiences;
    }

    public function addProfessionalExperience(ProfessionalExperience $professionalExperience): self
    {
        if (!$this->professionalExperiences->contains($professionalExperience)) {
            $this->professionalExperiences[] = $professionalExperience;
            $professionalExperience->setSlider($this);
        }

        return $this;
    }

    public function removeProfessionalExperience(ProfessionalExperience $professionalExperience): self
    {
        if ($this->professionalExperiences->removeElement($professionalExperience)) {
            // set the owning side to null (unless already changed)
            if ($professionalExperience->getSlider() === $this) {
                $professionalExperience->setSlider(null);
            }
        }

        return $this;
    }
}
