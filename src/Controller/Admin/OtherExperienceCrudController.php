<?php

namespace App\Controller\Admin;

use App\Entity\OtherExperience;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\UrlField;

class OtherExperienceCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return OtherExperience::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('title'),
            TextField::new('place', 'Localisation'),
            DateField::new('startDate', 'Date de début'),
            DateField::new('endDate', 'Date de fin'),
            UrlField::new('link', 'URL d\'un site en lien avec l\'activité'),
            TextEditorField::new('description', 'Description de l\'expérience'),
            AssociationField::new('slider', 'Slider'),
            AssociationField::new('file', 'Fichiers')
        ];
    }
    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInSingular('Expérience Annexe')
            ->setEntityLabelInPlural('Expériences Annexes');
    }
}