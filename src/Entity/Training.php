<?php

namespace App\Entity;

use App\Repository\TrainingRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TrainingRepository::class)
 */
class Training
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity=TrainingSlider::class, cascade={"persist", "remove"})
     */
    private $slider;

    /**
     * @ORM\Column(type="date")
     */
    private $startDate;

    /**
     * @ORM\Column(type="date")
     */
    private $endDate;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $dimension;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $schoolName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $place;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $schoolWebSite;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $compagny;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $compagnyWebSite;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $resultsObtained;

    /**
     * @ORM\ManyToMany(targetEntity=TrainingFile::class, inversedBy="trainings")
     */
    private $file;

    public function __construct()
    {
        $this->file = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSlider(): ?TrainingSlider
    {
        return $this->slider;
    }

    public function setSlider(?TrainingSlider $slider): self
    {
        $this->slider = $slider;

        return $this;
    }

    public function getStartDate(): ?\DateTimeInterface
    {
        return $this->startDate;
    }

    public function setStartDate(\DateTimeInterface $startDate): self
    {
        $this->startDate = $startDate;

        return $this;
    }

    public function getEndDate(): ?\DateTimeInterface
    {
        return $this->endDate;
    }

    public function setEndDate(\DateTimeInterface $endDate): self
    {
        $this->endDate = $endDate;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDimension(): ?string
    {
        return $this->dimension;
    }

    public function setDimension(string $dimension): self
    {
        $this->dimension = $dimension;

        return $this;
    }

    public function getSchoolName(): ?string
    {
        return $this->schoolName;
    }

    public function setSchoolName(?string $schoolName): self
    {
        $this->schoolName = $schoolName;

        return $this;
    }

    public function getPlace(): ?string
    {
        return $this->place;
    }

    public function setPlace(?string $place): self
    {
        $this->place = $place;

        return $this;
    }

    public function getSchoolWebSite(): ?string
    {
        return $this->schoolWebSite;
    }

    public function setSchoolWebSite(?string $schoolWebSite): self
    {
        $this->schoolWebSite = $schoolWebSite;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getCompagny(): ?string
    {
        return $this->compagny;
    }

    public function setCompagny(?string $compagny): self
    {
        $this->compagny = $compagny;

        return $this;
    }

    public function getCompagnyWebSite(): ?string
    {
        return $this->compagnyWebSite;
    }

    public function setCompagnyWebSite(?string $compagnyWebSite): self
    {
        $this->compagnyWebSite = $compagnyWebSite;

        return $this;
    }

    public function getResultsObtained(): ?string
    {
        return $this->resultsObtained;
    }

    public function setResultsObtained(?string $resultsObtained): self
    {
        $this->resultsObtained = $resultsObtained;

        return $this;
    }

    /**
     * @return Collection<int, TrainingFile>
     */
    public function getFile(): Collection
    {
        return $this->file;
    }

    public function addFile(TrainingFile $file): self
    {
        if (!$this->file->contains($file)) {
            $this->file[] = $file;
        }

        return $this;
    }

    public function removeFile(TrainingFile $file): self
    {
        $this->file->removeElement($file);

        return $this;
    }


}
