<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220311092230 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE training_training_file (training_id INT NOT NULL, training_file_id INT NOT NULL, INDEX IDX_FDAC1EC2BEFD98D1 (training_id), INDEX IDX_FDAC1EC24EF5E962 (training_file_id), PRIMARY KEY(training_id, training_file_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE training_training_file ADD CONSTRAINT FK_FDAC1EC2BEFD98D1 FOREIGN KEY (training_id) REFERENCES training (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE training_training_file ADD CONSTRAINT FK_FDAC1EC24EF5E962 FOREIGN KEY (training_file_id) REFERENCES training_file (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE training_training_file');
    }
}
