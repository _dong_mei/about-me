<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220310220543 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE training_slider_training_picture (training_slider_id INT NOT NULL, training_picture_id INT NOT NULL, INDEX IDX_8B543AAC4240F5CC (training_slider_id), INDEX IDX_8B543AAC54FB07D9 (training_picture_id), PRIMARY KEY(training_slider_id, training_picture_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE training_slider_training_picture ADD CONSTRAINT FK_8B543AAC4240F5CC FOREIGN KEY (training_slider_id) REFERENCES training_slider (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE training_slider_training_picture ADD CONSTRAINT FK_8B543AAC54FB07D9 FOREIGN KEY (training_picture_id) REFERENCES training_picture (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE training_slider_training_picture');
    }
}
