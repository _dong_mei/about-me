<?php

namespace App\Entity;

use App\Repository\TrainingPictureRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @ORM\Entity
 * @Vich\Uploadable
 */
class TrainingPicture
{
    public function __toString(): string
    {
        return $this->getImage();
    }
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    private $image;

    /**
     * @Vich\UploadableField(mapping="hobby_images", fileNameProperty="image")
     * @var File
     */
    private $imageFile;

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private $updatedAt;

    /**
     * @ORM\ManyToMany(targetEntity=TrainingSlider::class, mappedBy="picture")
     */
    private $trainingSliders;

    public function __construct()
    {
        $this->trainingSliders = new ArrayCollection();
    }

    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;

        // VERY IMPORTANT:
        // It is required that at least one field changes if you are using Doctrine,
        // otherwise the event listeners won't be called and the file is lost
        if ($image) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->updatedAt = new \DateTime('now');
        }
    }

    public function getImageFile()
    {
        return $this->imageFile;
    }

    public function setImage($image)
    {
        $this->image = $image;
    }

    public function getImage()
    {
        return $this->image;
    }

    public function getUpdatedAt(){
        return $this->updatedAt;
    }

    /**
     * @return Collection<int, TrainingSlider>
     */
    public function getTrainingSliders(): Collection
    {
        return $this->trainingSliders;
    }

    public function addTrainingSlider(TrainingSlider $trainingSlider): self
    {
        if (!$this->trainingSliders->contains($trainingSlider)) {
            $this->trainingSliders[] = $trainingSlider;
            $trainingSlider->addPicture($this);
        }

        return $this;
    }

    public function removeTrainingSlider(TrainingSlider $trainingSlider): self
    {
        if ($this->trainingSliders->removeElement($trainingSlider)) {
            $trainingSlider->removePicture($this);
        }

        return $this;
    }
}
