<?php

namespace App\Repository;

use App\Entity\OtherExperienceSlider;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method OtherExperienceSlider|null find($id, $lockMode = null, $lockVersion = null)
 * @method OtherExperienceSlider|null findOneBy(array $criteria, array $orderBy = null)
 * @method OtherExperienceSlider[]    findAll()
 * @method OtherExperienceSlider[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OtherExperienceSliderRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OtherExperienceSlider::class);
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add(OtherExperienceSlider $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function remove(OtherExperienceSlider $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    // /**
    //  * @return OtherExperienceSlider[] Returns an array of OtherExperienceSlider objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?OtherExperienceSlider
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
