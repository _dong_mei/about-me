<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220311074150 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE other_experience_slider (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE other_experience_slider_other_experience_picture (other_experience_slider_id INT NOT NULL, other_experience_picture_id INT NOT NULL, INDEX IDX_FA3FE2436779814 (other_experience_slider_id), INDEX IDX_FA3FE24DC876A52 (other_experience_picture_id), PRIMARY KEY(other_experience_slider_id, other_experience_picture_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE other_experience_slider_other_experience_picture ADD CONSTRAINT FK_FA3FE2436779814 FOREIGN KEY (other_experience_slider_id) REFERENCES other_experience_slider (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE other_experience_slider_other_experience_picture ADD CONSTRAINT FK_FA3FE24DC876A52 FOREIGN KEY (other_experience_picture_id) REFERENCES other_experience_picture (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE other_experience_slider_other_experience_picture DROP FOREIGN KEY FK_FA3FE2436779814');
        $this->addSql('DROP TABLE other_experience_slider');
        $this->addSql('DROP TABLE other_experience_slider_other_experience_picture');
    }
}
