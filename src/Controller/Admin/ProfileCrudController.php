<?php

namespace App\Controller\Admin;

use App\Entity\Profile;
use App\Field\VichImageField;
use Doctrine\DBAL\Types\DateType;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class ProfileCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Profile::class;
    }


    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('name', 'Nom'),
            TextField::new('firstName', 'Prénom'),
            VichImageField::new('imageFile', 'Image de Profile'),
            DateField::new('birthDate', 'Date de Naissance'),
            TextareaField::new('biography', 'Biographie'),
        ];
    }
    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInSingular('Profile de la cibles')
            ->setEntityLabelInPlural('Profiles de la cible');
    }
}
