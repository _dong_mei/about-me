<?php

namespace App\Controller\Admin;

use App\Entity\OtherExperienceFile;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Vich\UploaderBundle\Form\Type\VichFileType;

class OtherExperienceFileCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return OtherExperienceFile::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('title', 'Titre du fichier'),
            TextareaField::new('description'),
            TextField::new('fileFile', 'Fichier')
                ->setFormType(VichFileType::class)
                ->hideOnIndex(),
            ChoiceField::new('type', 'Type de fichier')
                ->setChoices([
                    'Certification'=>'Certification',
                    'Lettre de recommendation'=>'Lettre de recommendation',
                    'Diplôme'=>'Diplôme',
                    'Bulletin de notes'=>'Bulletin de notes',
                    'Résultats d\'examen'=> 'Résultats d\'examen',
                    "Autre"=>"Autre"
                ])
        ];
    }
}
