<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220311092002 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE professional_experience_professional_experience_file (professional_experience_id INT NOT NULL, professional_experience_file_id INT NOT NULL, INDEX IDX_912B5CF858832594 (professional_experience_id), INDEX IDX_912B5CF867B5B92C (professional_experience_file_id), PRIMARY KEY(professional_experience_id, professional_experience_file_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE professional_experience_professional_experience_file ADD CONSTRAINT FK_912B5CF858832594 FOREIGN KEY (professional_experience_id) REFERENCES professional_experience (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE professional_experience_professional_experience_file ADD CONSTRAINT FK_912B5CF867B5B92C FOREIGN KEY (professional_experience_file_id) REFERENCES professional_experience_file (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE professional_experience_professional_experience_file');
    }
}
