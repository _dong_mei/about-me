<?php

namespace App\Controller\Admin;

use App\Entity\ProfessionalExperiencePicture;
use App\Field\VichImageField;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class ProfessionalExperiencePictureCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return ProfessionalExperiencePicture::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            VichImageField::new('imageFile', 'Image'),
        ];
    }
    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInSingular('Image')
            ->setEntityLabelInPlural('Images');
    }
}
