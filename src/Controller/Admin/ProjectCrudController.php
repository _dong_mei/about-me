<?php

namespace App\Controller\Admin;

use App\Entity\Project;
use App\Field\VichImageField;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\UrlField;

class ProjectCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Project::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('title'),
            TextareaField::new('description'),
            VichImageField::new('imageFile', 'Image mise en avant'),
            UrlField::new('link', 'URL du projet'),
            DateField::new('date', 'Date'),
            ChoiceField::new('dimension', 'Dimension du projet')
            ->setChoices([
                'entreprise' => 'entreprise',
                'personnel' => 'personnel',
                'groupe'=>'groupe',
                'scolaire'=>'scolaire'
            ]),
            AssociationField::new('skills', 'Compétences exploitées')
        ];
    }
    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInSingular('Projet')
            ->setEntityLabelInPlural('Projets');
    }
}
