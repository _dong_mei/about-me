<?php

namespace App\Entity;

use App\Repository\OtherExperienceFileRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JetBrains\PhpStorm\Pure;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Entity\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @Vich\Uploadable
 * @ORM\Entity(repositoryClass=OtherExperienceFileRepository::class)
 */
class OtherExperienceFile
{
    #[Pure] public function __toString(): string
    {
        return $this->getTitle();
    }
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $type;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }


    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $file;

    /**
     * @Assert\File(maxSize="25M")
     * @Vich\UploadableField(mapping="training_files", fileNameProperty="file")
     * @var File
     */
    private $fileFile;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @ORM\ManyToMany(targetEntity=OtherExperience::class, mappedBy="file")
     */
    private $otherExperiences;

    public function __construct()
    {
        $this->otherExperiences = new ArrayCollection();
    }

    public function getFile(): ?string
    {
        return $this->file;
    }

    public function setFile(?string $file): self
    {
        $this->file = $file;

        return $this;
    }

    /**
     * @return File|null
     */
    public function getFileFile(): ?File
    {
        return $this->fileFile;
    }

    /**
     * @param File|null $fileFile
     */
    public function setFileFile(File $fileFile = null)
    {
        $this->fileFile = $fileFile;

        if($fileFile){
            $this->updatedAt = new \Datetime();
        }
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @return Collection<int, OtherExperience>
     */
    public function getOtherExperiences(): Collection
    {
        return $this->otherExperiences;
    }

    public function addOtherExperience(OtherExperience $otherExperience): self
    {
        if (!$this->otherExperiences->contains($otherExperience)) {
            $this->otherExperiences[] = $otherExperience;
            $otherExperience->addFile($this);
        }

        return $this;
    }

    public function removeOtherExperience(OtherExperience $otherExperience): self
    {
        if ($this->otherExperiences->removeElement($otherExperience)) {
            $otherExperience->removeFile($this);
        }

        return $this;
    }
}
