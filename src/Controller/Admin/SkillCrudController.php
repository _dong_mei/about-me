<?php

namespace App\Controller\Admin;

use App\Entity\Skill;
use App\Field\VichImageField;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\UrlField;

class SkillCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Skill::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('title', 'Titre de la compétence'),
            TextareaField::new('description', 'Détail de la compétence'),
            UrlField::new('link', 'Lien vers la documentation de la compétence'),
            VichImageField::new('imageFile')->onlyOnForms(),
            ChoiceField::new('field', 'Type de compétence')
            ->setChoices([
                'psychosociale' => 'psychosociale',
                'métier' => 'métier',
            ]),
            AssociationField::new('category', "Catégorie"),
            ChoiceField::new('grade', 'Niveau de maîtrise')
                ->setChoices([
                    'Débutant' => 'Débutant',
                    'Intermédiaire' => 'Intermédiaire',
                    'Avancé' => 'Avancé',
                    'Expert' => "Expert",
                ]),

        ];
    }
    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInSingular('Compétence')
            ->setEntityLabelInPlural('Compétences');
    }
}
