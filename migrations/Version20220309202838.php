<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220309202838 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE training_picture ADD training_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE training_picture ADD CONSTRAINT FK_CBE5DF87BEFD98D1 FOREIGN KEY (training_id) REFERENCES training (id)');
        $this->addSql('CREATE INDEX IDX_CBE5DF87BEFD98D1 ON training_picture (training_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE training_picture DROP FOREIGN KEY FK_CBE5DF87BEFD98D1');
        $this->addSql('DROP INDEX IDX_CBE5DF87BEFD98D1 ON training_picture');
        $this->addSql('ALTER TABLE training_picture DROP training_id');
    }
}
