<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220311073111 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE professional_experience_slider (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE professional_experience_slider_professional_experience_picture (professional_experience_slider_id INT NOT NULL, professional_experience_picture_id INT NOT NULL, INDEX IDX_F31F12747D1EFF3 (professional_experience_slider_id), INDEX IDX_F31F127E298BBFE (professional_experience_picture_id), PRIMARY KEY(professional_experience_slider_id, professional_experience_picture_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE professional_experience_slider_professional_experience_picture ADD CONSTRAINT FK_F31F12747D1EFF3 FOREIGN KEY (professional_experience_slider_id) REFERENCES professional_experience_slider (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE professional_experience_slider_professional_experience_picture ADD CONSTRAINT FK_F31F127E298BBFE FOREIGN KEY (professional_experience_picture_id) REFERENCES professional_experience_picture (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE professional_experience_slider_professional_experience_picture DROP FOREIGN KEY FK_F31F12747D1EFF3');
        $this->addSql('DROP TABLE professional_experience_slider');
        $this->addSql('DROP TABLE professional_experience_slider_professional_experience_picture');
    }
}
